$(document).ready(function() {
	//Hide content until the menu item is pressed
	$('.content').hide();
	$('#overview').show();
	
	// Get the height of the content div:
	var content_height = $('#content').height();
	
	$('#navbar').css('height', content_height + 'px');

});
