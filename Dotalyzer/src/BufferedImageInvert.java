import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.util.Hashtable;


public class BufferedImageInvert extends BufferedImage {

	public BufferedImageInvert(int width, int height, int imageType) {
		super(width, height, imageType);
		// TODO Auto-generated constructor stub
	}

	public BufferedImageInvert(int width, int height, int imageType,
			IndexColorModel cm) {
		super(width, height, imageType, cm);
		// TODO Auto-generated constructor stub
	}

	public BufferedImageInvert(ColorModel cm, WritableRaster raster,
			boolean isRasterPremultiplied, Hashtable<?, ?> properties) {
		super(cm, raster, isRasterPremultiplied, properties);
		// TODO Auto-generated constructor stub
	}
	
	public static void invertImage(BufferedImage img) {
		
		for (int x = 0 ; x < img.getWidth() ; x++) {
			for (int y = 0 ; y < img.getHeight() ; y++) {
				int rgba = img.getRGB(x, y);
				Color col = new Color(rgba);
				
				col = new Color(255 - col.getRed(),
								255 - col.getGreen(),
								255 - col.getBlue());
				
				img.setRGB(x, y, col.getRGB());
			}
		}
	}

}
