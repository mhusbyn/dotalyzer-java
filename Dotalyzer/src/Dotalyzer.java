import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.FlowLayout;
import java.awt.Canvas;
import java.awt.Rectangle;

public class Dotalyzer {
	
		
	public static void main(String[] args) {
		
		final JTextField txtSysPath = new JTextField();
		final JButton btnBrowse = new JButton("Browse");
		final JButton btnOpen = new JButton("Open");
		final JLabel lblImage = new JLabel("Image:");
		final JLabel lblEdgeDet = new JLabel("With Edge detect:");

		final File file;
		final BufferedImage img, edges;
		final CannyEdgeDetector detector = new CannyEdgeDetector(); // Borrowed from http://www.tomgibara.com/computer-vision/canny-edge-detector
		
		// Set up Frame with content.
		final JFrame frame = new JFrame("Dotalyzer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		frame.setLocationByPlatform(true);
		
		txtSysPath.setEditable(false);
		txtSysPath.setText("System path");
		frame.getContentPane().add(txtSysPath);
		txtSysPath.setColumns(10);
		
		frame.getContentPane().add(btnBrowse);
		frame.getContentPane().add(btnOpen);		
		frame.getContentPane().add(lblImage);
		frame.getContentPane().add(lblEdgeDet);
		
		lblImage.setHorizontalTextPosition(SwingConstants.LEADING);
		lblEdgeDet.setHorizontalTextPosition(SwingConstants.LEADING);
		
		frame.pack();
		frame.setSize(200, 200);
		frame.setVisible(true);
		
		
		final JFileChooser fc = new JFileChooser();
		final FileNameExtensionFilter filter = new FileNameExtensionFilter("GIF, PNG, JPEG, BMP, and WBMP", "gif", "png", "jpg", "jpeg", "bmp", "wbmp");
		fc.setFileFilter(filter);
		
		
		btnBrowse.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Open file browser
				int returnVal = fc.showOpenDialog(btnBrowse);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					
					// Add a file object whose path is shown in txtSysPath
					file = fc.getSelectedFile();
					txtSysPath.setText(file.getAbsolutePath());
					
					System.out.println("We have a file selected: " + file);
				}
			}
		});
		
		btnOpen.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Creates the image from the file path
				try {
					img = ImageIO.read(file);
					
					System.out.println("We have an image selected: " + img);
				}
				catch (IOException exc) {
					System.out.println("Error: IOException\n" + exc);
				}
				catch (IllegalArgumentException exc) {
					System.out.println("Error: Illegal argument\n" + exc);
				}
				
				if (img != null) {
					detector.setSourceImage(img);
					detector.process();
					
					edges = detector.getEdgesImage();
					BufferedImageInvert.invertImage(edges);
					
					lblImage.setIcon(new ImageIcon(img));
					lblEdgeDet.setIcon(new ImageIcon(edges));
					
					frame.setSize(120 + img.getWidth() + edges.getWidth(), 120 + img.getHeight() + edges.getHeight());					
					
					System.out.println("Modified the canvas.");
					System.out.println("Random int pixel: " + edges.getRGB(0, 0));
					
					
				}
			}
		});
		

	}

}
